
# Top-level entities

```
Construction
    Stage
        Task [
            Construction Supplies
            Workforce
            Equipment
            Machines
            Supplies
        ]
```

```
Supplier
    Workforce
    Construction Supplies
    Equipment
    Machines
    Supplies
        Fuel, Electricity, Engine Oil, etc.
```

```
Pricing
    TODO
```

# Planner

```
Planner = [Stage] + [Previous Stage, Start Date, Time Estimate]
```
```
Construction Supplies Cost Estimate
    Code, Name, +[Supplier, Cost]
```

```
Workforce Cost Estimate
    Code, Name, +[Supplier, Cost]
```

```
Equipment Cost Estimate
    Code, Name, +[Supplier, Cost]
```

```
Machines Cost Estimate
    Code, Name, +[Supplier, Cost]
```

TODO: Supplies Estimate

# Schedule Reports

```
Construction Supplies Schedule -> Code, Name, +[Amount, Date]
```

```
Equipment & Equipment Supplies Schedule -> Code, Name, +[Amount, Date]
```

```
Machines & Machine Supplies Schedule -> Code, Name, +[Amount, Date]
```

# Task Sign-off

```
Date + Stage:Task [
    Construction Supplies, + Supplier
    Workforce, + Supplier
    Equipment, + Supplier
    Machines, + Supplier
    Machines & Equipment Supplies, + Supplier
]
```
