from typing import List
import json


class JSONSerializable:
    def to_json(self) -> str:
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4,
                          ensure_ascii=False)


class Estimate(JSONSerializable):
    def __init__(self, builders: float, engineers: float):
        self.builders = builders
        self.engineers = engineers


class Cost(JSONSerializable):
    def __init__(self, salary: float, machinery: float, maintenance: float):
        self.salary = salary
        self.machinery = machinery
        self.maintenance = maintenance


class Supplies(JSONSerializable):
    def __init__(
        self, code: str, description: str, unit_of_measure: str, amount: float
    ):
        self.code = code
        self.description = description
        self.unit_of_measure = unit_of_measure
        self.amount = amount


class Task(JSONSerializable):
    def __init__(
        self,
        code: str,
        description: str,
        unit_of_measure: str,
        amount: float,
        cost: Cost,
        estimate: Estimate,
    ):
        self.code = code
        self.description = description
        self.unit_of_measure = unit_of_measure
        self.amount = amount
        self.estimate = estimate
        self.cost = cost
        self.requirements = []


class Stage(JSONSerializable):
    def __init__(self, name: str):
        self.name = name
        self.tasks: List[Task] = []


class Project(JSONSerializable):
    def __init__(self, name: str):
        self.name = name
        self.stages = []
