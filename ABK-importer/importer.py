import argparse

import openpyxl
from openpyxl.worksheet.worksheet import Worksheet

from planner import Cost, Estimate, Project, Supplies, Stage, Task

parser = argparse.ArgumentParser()
parser.add_argument("excel")
args = parser.parse_args()

wb = openpyxl.load_workbook(
    filename=args.excel, read_only=True, data_only=True)

plan_ws = wb.worksheets[0]


class ABKParser:

    __ABK_headers = {
        1: "index",
        2: "code",
        3: "description",
        4: "units-of-measure",
        5: "amount",

        6: "cost-per-unit",
        7: "machinery-cost-per-unit",

        8: "total-cost",
        9: "total-salary",
        10: "total-machinery-cost",

        11: "estimate-per-unit",
        12: "total-estimate"
    }

    __ABK_prefixes = {
        "stage_name": "роздiл ",
        "task_code": "е",
        "supplies_code": "с"
    }

    __CONFIG = {
        "header_lookup": {
            "max_row": 30,
            "max_column": 'E',
        }
    }

    def __init__(self, ws: Worksheet):
        self.ws = ws
        self.header_start_column = self.__find_header()
        self.header = self.read_header()
        self.row_id = 1

    def first_row(self) -> None:
        self.row_id = 1

    def next_row(self) -> None:
        self.row_id += 1

    def __eof(self) -> bool:
        return self.row_id >= self.ws.max_row

    def __cell_value_by_header(self, header: str):
        return self.ws[f'{self.header[header]}{self.row_id}'].value

    def __subcell_value_by_header(self, header: str):
        return self.ws[f'{self.header[header]}{self.row_id + 1}'].value

    def __sanitize(self, value: str):
        return value.replace('\n', ' ').strip()

    def __index(self) -> str:
        return self.__cell_value_by_header("index")

    def __code(self) -> str:
        return self.__cell_value_by_header("code")

    def __description(self) -> str:
        return self.__cell_value_by_header("description")

    def __units_of_measure(self) -> str:
        return self.__cell_value_by_header("units-of-measure")

    def __amount(self) -> float:
        return self.__cell_value_by_header("amount")

    def __salary(self) -> float:
        return self.__cell_value_by_header("total-salary")

    def __machinery(self) -> float:
        return self.__cell_value_by_header("total-machinery-cost")

    def __maintenance(self) -> float:
        return self.__subcell_value_by_header("total-machinery-cost")

    def __builders(self) -> float:
        return self.__cell_value_by_header("total-estimate")

    def __engineers(self) -> float:
        return self.__subcell_value_by_header("total-estimate")

    def __is_header_row(self, start_column=None) -> bool:
        if not start_column:
            start_column = self.header_start_column

        cell_range = self.ws[f'{start_column}{self.row_id}':
                             f'{chr(ord(start_column)+24)}{self.row_id}']
        value_range = [x.value for x in cell_range[0] if x.value]
        header = [*self.__ABK_headers]
        return value_range[0:len(self.__ABK_headers)] == header

    def __find_header(self) -> str:
        for c in range(ord('A'), ord(self.__CONFIG["header_lookup"]["max_column"])):
            self.first_row()
            while self.row_id < self.__CONFIG["header_lookup"]["max_row"]:
                column = chr(c)
                if self.__is_header_row(column):
                    return column
                self.next_row()

        raise RuntimeError("no header found")

    def read_header(self) -> dict:
        result = {}
        for c in range(ord(self.header_start_column), ord(self.header_start_column) + 24):
            column = chr(c)
            cell_value = self.ws[f'{column}{self.row_id}'].value
            if not cell_value:
                continue

            result[self.__ABK_headers[cell_value]] = column
            if len(result) == len(self.__ABK_headers):
                break

        self.next_row()
        return result

    def __is_stage_name(self, name: str) -> bool:
        return name and name.lstrip().casefold().startswith(self.__ABK_prefixes["stage_name"])

    def __is_task_code(self, code: str) -> bool:
        return code and code.lstrip().casefold().startswith(self.__ABK_prefixes["task_code"])

    def __is_supplies_code(self, code: str) -> bool:
        return code and code.lstrip().casefold().startswith(self.__ABK_prefixes["supplies_code"])

    def __is_stage_row(self) -> bool:
        return (not self.__is_header_row()) and (not self.__code()) and self.__is_stage_name(self.__description())

    def __is_task_row(self) -> bool:
        return (not self.__is_header_row()) and self.__is_task_code(self.__code())

    def __is_supplies_row(self) -> bool:
        return (not self.__is_header_row()) and self.__is_supplies_code(self.__code())

    def __skip_to_stage(self) -> bool:
        while (not self.__eof()) and (not self.__is_stage_row()):
            if self.__is_task_row() or self.__is_supplies_row():
                return False
            self.next_row()

        return not self.__eof()

    def read_project(self) -> Project:
        result = Project("TODO")
        while stage := self.read_stage():
            result.stages.append(stage)

        return result

    def read_stage(self) -> Stage:
        result = None
        if self.__skip_to_stage():
            result = Stage(
                self.__sanitize(self.__description())
            )
            self.next_row()

            while task := self.read_task():
                result.tasks.append(task)

        return result

    def __skip_to_task(self) -> bool:
        while (not self.__eof()) and (not self.__is_task_row()):
            if self.__is_stage_row() or self.__is_supplies_row():
                return False
            self.next_row()

        return not self.__eof()

    def read_task(self) -> Task:
        result = None
        if self.__skip_to_task():
            result = Task(
                self.__sanitize(self.__code()),
                self.__sanitize(self.__description()),
                self.__units_of_measure(),
                self.__amount(),
                Cost(
                    self.__salary(),
                    self.__machinery(),
                    self.__maintenance()
                ),
                Estimate(
                    self.__builders(),
                    self.__engineers()
                )
            )
            self.next_row()

            while supplies := self.read_supplies():
                result.requirements.append(supplies)

        return result

    def __skip_to_supplies(self) -> bool:
        while (not self.__eof()) and (not self.__is_supplies_row()):
            if self.__is_stage_row() or self.__is_task_row():
                return False
            self.next_row()

        return not self.__eof()

    def read_supplies(self) -> Supplies:
        result = None
        if self.__skip_to_supplies():
            result = Supplies(
                self.__sanitize(self.__code()),
                self.__sanitize(self.__description()),
                self.__units_of_measure(),
                self.__amount()
            )
            self.next_row()

        return result


parser = ABKParser(plan_ws)
project = parser.read_project()

print(project.to_json())
